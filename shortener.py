

import webapp

#Formulario
formulario = """
    <form action="" method="POST">
        url:
        <input type="text" name="url"><br>
        short:
        <input type="text" name="short"><br>
        <input type="submit" value="Enviar">
        </form>
"""

class shortener (webapp.webApp):

    #Diccionario donde guardaremos las URLS que vamos acortar
    diccionario = {}

    def parse(self, request): #Trozeamos para sacar lo que nos interesa

        Metodo = request.split(' ', 2)[0]
        Recurso = request.split(' ', 2)[1]
        Cuerpo = request.split('\r\n\r\n', 1)[1]

        return (Metodo, Recurso, Cuerpo)


    def process(self, petition):    #Funcione el servidor

        Metodo, Recurso, Cuerpo = petition   #Pasamos los parametros del parse al process


        if Recurso == "/" and Metodo == "GET":   #Empezamos con el metodo GET aparezca el formulario y listado de URLs reales acotadas
            httpcode = "200 OK"
            htmlBody = "<html><body>" + formulario + "<br><br><br>" + \
            "<p>Listado URLs acotadas actuales:</p><br>" + str(self.diccionario) + \
            "</body></html>"
            return (httpcode, htmlBody)
        elif Recurso == "/" and Metodo == "POST":             # Ejemplo ---> url=https%3A%2F%2Fwww.urjc.es%2F&short=uni
            url = Cuerpo.split('&')[0]  # url=https%3A%2F%2Fwww.urjc.es
            url1 = url.split('=')[1]  # https%3A%2F%2Fwww.urjc.es
            urlfinal = url1.replace('%3A', ':').replace('%2F', '/')  # https://www.urjc.es (resultado final)
            short = Cuerpo.split('short=')[1]

            if urlfinal == "" and short == "":  #ERROR si se envia formulario sin nada
                httpcode = "404 Not Found"
                htmlBody = "<html><body>ERROR Rellena ambos campos" + \
                           "</body></html>"

                return (httpcode, htmlBody)

            if urlfinal[0:7] != "http://" and urlfinal[0:8] != "https://":    #Excepcion en el caso que venga sin el "https:\\"
                    urlfinal = ("https://" + urlfinal)

            if urlfinal != "" and short != "":  #Convertimos a Url los campos de url y short
                    self.diccionario[short] = urlfinal
                    httpcode= "200 OK"
                    htmlBody ="<html><body>url: " + "<a href='" + urlfinal + \
                    "'>" + urlfinal + "</a><br>" + "Short: " + "<a href='" + self.diccionario[short] + \
                    "'>" + short + "</a></body></html>"
                    return (httpcode, htmlBody)

            else:
                    httpcode = "404 Not Found"
                    htmlBody = "<html><body>ERROR Rellena ambos campos" + \
                    "</body></html>"

                    return (httpcode, htmlBody)

        elif Recurso[1:] in self.diccionario:  # HTTP REDIRECT
            httpcode = "302 Found"
            htmlBody = "<html><body><meta http-equiv='refresh' content='1;" + \
            "url= " + self.diccionario[Recurso[1:]] + "'></body></html>" + "\r\n"

            return (httpcode, htmlBody)
        else:
            #CONTROL DE ERRORES
            httpcode = "404 Not Found"
            htmlBody = "<html><body>Metodo no soportado</body></html>"
            return (httpcode,htmlBody)


if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)



